from svmutil import *
# Read data in LIBSVM format
y1, x1 = svm_read_problem('../data_test_2/training');
print("Lecture du fichier de train terminé");
m = svm_train(y1, x1, '-c 16 -g 1');
print("Training du fichier de train terminé");

y2, x2 = svm_read_problem('../data_test_2/testing');
print("Lecture du fichier de test terminé");
p_label, p_acc, p_val = svm_predict(y2,x2, m);
print("Testing du fichier de test terminé");

