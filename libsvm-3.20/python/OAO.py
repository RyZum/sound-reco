from operator import add
from svmutil import *
from os import listdir,remove
from pprint import pprint
import sys; 
sys.path.append('C:\\Users\\montesson\\Documents\\Nicolas\\x\\travail\\PSC\\git\\libsvm-3.20\\python');

"""ids = {}
ids["[1]_Impromptu_op9n4.svm"] = "1"
ids["[2]_Lettre_Elise.svm"] = "2"
ids["[2]_Lettre_Elise2.svm"] = "-2"
ids["[3]_Liebestraum.svm"] = "3"
ids["[4]_Marche_turque.svm"] = "4"
ids["[5]_Sonate_K545_2mvt.svm"] = "5"
ids["[6]_Striding.svm"] = "6"
ids["[7]_Celite_springs.svm"] = "7"
ids["[8]_Valse_op64n2_CHOPIN_(2).svm"] = "8"
ids["[9]_Glassy_Eyes.svm"] = "9"
ids["[10]_Must_not_languish.svm"] = "10"
ids["[4]_Marche_turque_avec_fausses_notes_lol.svm"] = "-4"
ids["[4]_Marche_turque_avec_un_peu_de_bruit.svm"] = "-5"
"""
class OAO:
    def __init__(self,fichier1,fichier2):
        self.clear();
        y1, x1 = svm_read_problem(fichier1);
        y1, x1 = map(add,(y1,x1),svm_read_problem(fichier2));
        m = svm_train(y1, x1, '-c 2 -b 1');
        svm_save_model('models\\'+fichier1+'#'+fichier2,m);
        with open('models\\data', 'w') as f:
            f.write(fichier1+'\n');
            f.write(fichier2+'\n');

    def clear(self):
        for filename in listdir('.\\models'):
            remove('.\\models\\'+filename)

    def addMusic(self,fichier):
        y0, x0 = svm_read_problem(fichier);
        with open('models\\data','a+') as f:
            f.seek(0);
            for x in f:
                x = x.strip();
                y1,x1 = map(add,(y0,x0),svm_read_problem(x));
                m = svm_train(y1, x1, '-c 4');
                svm_save_model('models\\'+fichier+'#'+x,m);
            f.write(fichier+'\n');

    def identify(self,fichier):
        results = {};
        yTest, xTest = svm_read_problem(fichier);
        for filename in listdir('.\\models'):
            if filename != "data":
                #print("-------------------")
                #print(filename)
                m = svm_load_model('.\\models\\'+filename);
                p_label, p_acc, p_val = svm_predict(yTest,xTest,m);
                a,b = filename.split("#");
                #a = ids[a];
               # b = ids[b];
                #print(p_label,p_val)
                for i in p_label :
                    if str(int(i)) == a :
                        temp = results.setdefault(a,0)
                        results[a] = temp + 1;
                    if str(int(i)) == b :
                        temp = results.setdefault(b,0)
                        results[b] = temp + 1;
                    if str(int(i)) != a and str(int(i)) != b :
                        print("Etrange... p_label contient une valeur " + str(int(i)) + " qui n'est ni " + str(a)+ " ni "+str(b))
                print(results);
			


    def removeMusic(self, fichier):
        for filename in listdir('.\\models'):
            if filename != "data":
                a,b = filename.split('#')
                if a==fichier or b==fichier :
                    remove('.\\models\\'+filename)
