from operator import add
from svmutil import *
from os import listdir,remove,path
from pprint import pprint
from pdb import set_trace
import sys; 
import json;
import multiprocessing;
import concurrent.futures;
sys.path.append('C:\\Users\\montesson\\Documents\\Nicolas\\x\\travail\\PSC\\git\\libsvm-3.20\\python');


def create(fichier1,fichier2, folder = '.'):
        clear(folder);
        if (fichier2!=""):
                y1, x1 = svm_read_problem(fichier1);
                y1, x1 = map(add,(y1,x1),svm_read_problem(fichier2));
                m = svm_train(y1, x1, '-c 4 -b 1 -q');
                print("Generating file ... " + path.join(folder,'models',path.basename(fichier1)+'#'+path.basename(fichier2)));
                svm_save_model(path.join(folder,'models',path.basename(fichier1)+'#'+path.basename(fichier2)),m);
                with open(path.join(folder,'models','data'), 'w') as f:
                        f.write(path.basename(fichier1)+'\n');
                        f.write(path.basename(fichier2)+'\n');
        else:
                with open(path.join(folder,'models','data'), 'w') as f:
                        f.write(path.basename(fichier1)+'\n');



def clear(folder = '.'):
        for filename in listdir(path.join(folder,'models')):
                remove(path.join(folder,'models',filename))

def addMusic(fichier, folder = '.'):
        y0, x0 = svm_read_problem(fichier);
        print("Generating necessary files for " + fichier);
        with open(path.join(folder,'models','data'),'a+') as f:
                f.seek(0);
                for x in f:
                        x = x.strip();
                        y1,x1 = map(add,(y0,x0),svm_read_problem(path.join(folder,x)));
                        m = svm_train(y1, x1, '-c 4 -b 1');
                        #print("Generating file ... " + path.join(folder,'models',path.basename(fichier)+'#'+path.basename(x)));
                        svm_save_model(path.join(folder,'models',path.basename(fichier)+'#'+path.basename(x)),m);
                f.write(path.basename(fichier)+'\n');

def identifySpecificMusic(filename,folder, yTest, xTest):
        m = svm_load_model(path.join(folder,'models',filename));
        p_label, p_acc, p_val = svm_predict(yTest,xTest,m, '-b 1 -q');
        a,b = filename.split("#");
        return p_label, p_val, a, b

def identify(fichier, folder = '.'):
        yTest, xTest = svm_read_problem(fichier);
        pointsGagnes = {};   #score cumulé en % de chaque 1v1
        matchsGagnes = {};   #nombre de 1v1 gagnés
        pointsGagnesProba = {};   #En tenant en compte de la précision de chaque placement de vecteur
        matchsGagnesProba = {};
        
        futures = [];
        print("Executing on " + str(multiprocessing.cpu_count()) + " threads");
        with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
                for filename in listdir(path.join(folder,'models')):
                        if filename != "data":
                                futures.append(executor.submit(identifySpecificMusic, filename, folder, yTest, xTest));
                for future in concurrent.futures.as_completed(futures):
                        try:
                                p_label, p_val, a, b = future.result()
                        except Exception as exc:
                                print('%r generated an exception: %s' % (filename, exc))


                        results = {};   #Le tableau avec les résultats a vs b. Seuls resultas[a] et results[b] existent.
                        resultsProba = {}
                        results[a] = 0
                        results[b] = 0
                        resultsProba[a] = 0
                        resultsProba[b] = 0
                        for i,j in zip(p_label,p_val) :
                                if str(int(i)) == a :
                                        results[a] += 1;
                                        resultsProba[a] += max(j);
                                        resultsProba[b] += min(j);
                                if str(int(i)) == b :
                                        results[b] += 1;
                                        resultsProba[a] += min(j);
                                        resultsProba[b] += max(j);
                                if str(int(i)) != a and str(int(i)) != b :
                                        print("Etrange... p_label contient une valeur " + str(int(i)) + " qui n'est ni " + str(a)+ " ni "+str(b))
                        #print(results)
                        pointsGagnes[a] = pointsGagnes.setdefault(a,0) + results[a]/(results[a]+results[b])
                        pointsGagnes[b] = pointsGagnes.setdefault(b,0) + results[b]/(results[a]+results[b])
                        pointsGagnesProba[a] = pointsGagnesProba.setdefault(a,0) + resultsProba[a]/(resultsProba[a]+resultsProba[b])
                        pointsGagnesProba[b] = pointsGagnesProba.setdefault(b,0) + resultsProba[b]/(resultsProba[a]+resultsProba[b])

                        if results[a]>results[b]:
                                matchsGagnes[a] = matchsGagnes.setdefault(a,0) + 1
                        if results[a]<results[b]:
                                matchsGagnes[b] = matchsGagnes.setdefault(b,0) + 1
                        if resultsProba[a]>resultsProba[b]:
                                matchsGagnesProba[a] = matchsGagnesProba.setdefault(a,0) + 1
                        if resultsProba[a]<resultsProba[b]:
                                matchsGagnesProba[b] = matchsGagnesProba.setdefault(b,0) + 1
                                
                        #print(results);
        print("\nPoints[-]") # nombre de points : +1/0 par vecteurs reconnu/non reconnu
        pprint(sorted(pointsGagnes.items(),key = lambda a : (a[1],a[0]), reverse = True));
        print("\nScore[-]") # nombre de comparaison 1v1 gagnés en calculant avec Points[-]
        pprint(sorted(matchsGagnes.items(),key = lambda a : (a[1],a[0]), reverse = True));
        print("\nPoints[+]") #nombre de points : +p/+1-p où p est la proba que le vecteur soit reconnu
        pprint(sorted(pointsGagnesProba.items(),key = lambda a : (a[1],a[0]), reverse = True));
        print("\nScore[+]") # nombre de comparaison 1v1 gagnés en calculant avec Points[+]
        pprint(sorted(matchsGagnesProba.items(),key = lambda a : (a[1],a[0]), reverse = True));
        print("\n")
        sortie = open(fichier + "_score","w");
        sortie.write(json.dumps({"Points[-]":pointsGagnes, "Score[-]":matchsGagnes,"Points[+]":pointsGagnesProba,"Score[+]":matchsGagnesProba}));       


def removeMusic(fichier, folder= '.'):
        fichier = str(fichier);
        for filename in listdir(path.join(folder ,'models')):
                if filename != "data":
                        a,b = filename.split('#')
                        if a==fichier or b==fichier :
                                remove(path.join(folder,'models',filename))
        texte = "";
        with open(path.join(folder,'models','data'),'r') as f:
                for x in f:
                        if x!=fichier:
                                texte += x;
        with open(path.join(folder,'models','data'),'w') as f:
                f.write(texte);
