import OAO 
from os import listdir,remove,path,mkdir,rename

class OAOManager():
    def __init__(self):
        self.SVM = {};
    
    def createSVM(self,folder):
        FileList = [ file for file in listdir(folder) if path.isfile(path.join(folder, file))];
        FileList = list(map(lambda x : path.join(folder,x),FileList));
        if not(path.exists(path.join(folder,'models'))):
            mkdir(path.join(folder,'models'))

        if (len(FileList)>1):
            OAO.create(FileList[0],FileList[1],folder);
        else:
            OAO.create(FileList[0],"", folder);
            
        for filename in FileList[2:]:
            if path.isfile(filename):
                OAO.addMusic(filename,folder);

    def testSVM(self,folder):
        FileList = listdir(path.join(folder,'test'));
        FileList = list(map(lambda x : path.join(folder,'test',x),FileList));
        for filename in FileList:
            if ("_" not in filename):
                print("################################################################")
                print(filename)
                print("################################################################")
                OAO.identify(filename,folder);

    def addMusic(self,folder): #Place the music to add in the subfolder add
        FileList = listdir(path.join(folder,"add"));
        FileList = list(map(lambda x : path.join('.',folder,"add",x),FileList));
        for filename in FileList:
            if path.isfile(filename):
                OAO.addMusic(filename,folder);
                rename(filename, path.join(folder, path.basename(filename)))
                
    @staticmethod
    def removeMusic(fichier,folder):
       OAO.removeMusic(fichier,folder);
