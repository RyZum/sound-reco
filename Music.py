from wavparser.wav2times import create_times
from wavparser.generator import create_csv 
from tools.conversion import convert
import tools.dictionnaire 
from musicalDescriptors.hpd import extractHighestMagnitudes
from pydub import AudioSegment
from musicalDescriptors.pcp_2 import extractPCP
import json
import multiprocessing;
import concurrent.futures;

import sys
import os
current_file = os.path.abspath(__file__)
current_dir = os.path.split(current_file)[0]
sys.path.append(os.path.join(current_dir, "libsvm-3.20", "SVM_V2"))
sys.path.append(os.path.join(current_dir, "libsvm-3.20", "python"))

from OAOManager import OAOManager




#par Antoine Colin

#valeurs typiques pour overlap, dimension : 12,60

def convert_mp3_to_wav(fichier): #Convertit un fichier, s'il est .mp3, en .wav stéréo (inutilisable par wavparser donc)
	if fichier.endswith(".mp3"):
		song = AudioSegment.from_mp3(fichier)
		nom_wav = os.path.splitext(fichier)[0]+".wav"
		song.export(nom_wav,parameters= ["-ac", "1"],format = "wav")
		return (nom_wav)
	else :
		return(fichier)
		
def boucle(fichier_source, dossier, id, i):
	fichier_csv = os.path.join("data", "pcp",dossier, str(id)+str(i)+".csv")
	source_mp3 = convert_mp3_to_wav(fichier_source)
		
	try :
		if os.path.getmtime(fichier_csv)<os.path.getmtime(fichier_source) :
			extractPCP(source_mp3, fichier_csv)
		else :
			print("Fichier csv déjà créé")	
	except OSError :
		extractPCP(source_mp3, fichier_csv)
	return(fichier_csv)


def add_music_group_pcp(overlap, dimension, nom, liste_fichiers) : #ajoute un groupe de musiques (plusieurs interprétations d'un même morceau)
	#ajoute l'id au dictionnaire
	d = tools.dictionnaire.load()
	reverse =  { value:key for key, value in d.items() }
	if d :
		id = max(d.keys())+1
	else :
		id = 1
	if nom not in d.values():
		d[id] = nom
		tools.dictionnaire.save(d)
		deja_present = False
	else :
		id = reverse[nom]
		deja_present = True
	fichiers_csv = []
	sortie = os.path.join("data", "pcp", "csv", str(id)+".csv")
	with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
		futures=[]
		for i, fichier_source in enumerate(liste_fichiers):
			futures.append(executor.submit(boucle, fichier_source, "csv", id, i))
		for future in concurrent.futures.as_completed(futures):
			fichier_csv = future.result()
			fichiers_csv.append(fichier_csv)	
		
	with open(sortie, "w") as out:
		for fichier in fichiers_csv:
			with open(fichier, "r") as f:
				out.writelines(f)
			os.remove(fichier)
	#On a notre fichier csv, on veut notre fichier svm.
	svm_creee = False
	dossier_SVM = os.path.join("data", "pcp", "svm")
	fichier_svm = os.path.join("data", "pcp", "svm", str(id))
	if os.path.isdir(os.path.join(dossier_SVM, "models")):
		svm_creee = True
		fichier_final = os.path.join("data", "pcp", "svm", "add", str(id))
		convert(sortie, fichier_final , overlap, dimension, id)
		if(deja_present):
			with open(fichier_final, "a") as f1:
				with open(fichier_svm, "r") as f2:
					f1.writelines(f2) 		
	else :
		convert(sortie, fichier_svm, overlap, dimension, id)
	#On ajoute maintenant à la SVM
	a = OAOManager()
	if svm_creee :
		if deja_present:
			a.removeMusic(str(id), dossier_SVM)
		a.addMusic(dossier_SVM)
	else :
		a.createSVM(os.path.join("data", "pcp", "svm"))
		
def remove_music_group_pcp(nom):
	d = tools.dictionnaire.load()
	reverse =  { value:key for key, value in d.items() }
	if isinstance(nom, str):
		if nom not in d.values():
			raise LookupError("Le morceau "+nom+" n'est pas dans la base de données")
		else:
			id = reverse[nom]
			del d[id]
			tools.dictionnaire.save(d)
			dossier_SVM = os.path.join("data", "pcp", "svm")
			OAOManager.removeMusic(id, dossier_SVM)
	elif isinstance(nom, int) :
		if nom not in d.keys():
			raise LookupError("Le morceau "+str(nom)+" n'est pas dans la base de données")
		del d[nom]
		tools.dictionnaire.save(d)
		dossier_SVM = os.path.join("data", "pcp", "svm")
		OAOManager.removeMusic(nom, dossier_SVM)
		
#faire un dossier atester dans data/wavparser
		
def test_music_group_pcp(overlap, dimension, liste_fichiers):
	i=0;
	fichiers_csv = []
	id = -1;
	csv_test= os.path.join("data", "pcp", "atester", str(id)+".csv")
	with concurrent.futures.ProcessPoolExecutor(max_workers=multiprocessing.cpu_count()) as executor:
		futures=[]
		for i, fichier_source in enumerate(liste_fichiers):
			futures.append(executor.submit(boucle, fichier_source, "csv", id, i))
		for future in concurrent.futures.as_completed(futures):
			fichier_csv = future.result()
			fichiers_csv.append(fichier_csv)
	with open(csv_test, "w") as out:
		for fichier in fichiers_csv:
			with open(fichier, "r") as f:
				out.writelines(f)
			os.remove(fichier)
	#On a notre fichier csv, on veut notre fichier svm.

	svm_creee = False
	dossier_SVM = os.path.join("data", "pcp", "svm")
	fichier_svm_test = os.path.join("data", "pcp", "svm", "test", str(id)) 
	if os.path.isdir(os.path.join(dossier_SVM, "models")):
		svm_creee = True
	convert(csv_test, fichier_svm_test, overlap, dimension, id)

	a = OAOManager()
	a.testSVM(dossier_SVM)
	with open(os.path.join(dossier_SVM, "test", "-1_score")) as f:
		res = json.load(f)
		return res
	
#Il faut gérer l'id
		

	

def add_dossier(overlap, dimension):
	folder = os.path.join("data", "wav")
	FileList = os.listdir(folder);
	for filename in FileList:
			id = add_id(os.path.splitext(filename)[0])
			add_music_hpd(id, overlap, dimension)
        

def add_id(nom_chanson): #nom_chanson doit être une string (nom du fichier sans le .wav), le fichier doit être dans data/wav
	
	
	d = tools.dictionnaire.load()
	if d :
		id = max(d.keys())+1
	else :
		id = 1
	if not nom_chanson in d.values():
		d[id] = nom_chanson
		fichier_wav1 = os.path.join("data", "wav", nom_chanson+".wav")
		fichier_wav2 = os.path.join("data", "wav", str(id)+".wav")
		os.rename(fichier_wav1, fichier_wav2)
		print(d)
		tools.dictionnaire.save(d)
	else :
		print(nom_chanson + "a déjà été ajoutée")
	return id
	
	
	
	
	

def add_music_wavparser(id, overlap, dimension):
	fichier_wav = os.path.join("data", "wav", str(id)+".wav")
	fichier_times = os.path.join("data", "wavparser", "times", str(id)+".times")
	fichier_csv = os.path.join("data","wavparser", "csv",  str(id)+".csv")
	fichier_svm = os.path.join("data","wavparser", "svm", str(id)+".svm")
	
	try :	
		if os.path.getmtime(fichier_times)<os.path.getmtime(fichier_wav) :
			create_times(fichier_wav, fichier_times)
		else :
			print("Fichier times déjà créé")	
	except OSError : 
		create_times(fichier_wav, fichier_times)
		
	try :
		if os.path.getmtime(fichier_csv)<os.path.getmtime(fichier_times) :
			create_csv(fichier_wav, fichier_times, fichier_csv)
		else :
			print("Fichier csv déjà créé")	
	except OSError :
		create_csv(fichier_wav, fichier_times, fichier_csv)
	try :
		if os.path.getmtime(fichier_svm)<os.path.getmtime(fichier_csv) :
			convert(fichier_csv, fichier_svm, overlap, dimension, id)
		else :
			print("Fichier svm déjà créé")	
	except OSError :
		convert(fichier_csv, fichier_svm, overlap, dimension, id)
		
		

def add_music_hpd(id, overlap, dimension):	
	fichier_wav = os.path.join("data", "wav", str(id)+".wav")
	fichier_csv = os.path.join("data","hpd", "csv",  str(id)+".csv")
	dossier_svm = os.path.join("data","hpd", "svm")
	if os.path.isdir(os.path.join(dossier_svm, "models")):
		fichier_svm = os.path.join(dossier_svm,"add", str(id)+".svm")
	else:
		fichier_svm = os.path.join(dossier_svm, str(id)+".svm")
	
	try :	
		if os.path.getmtime(fichier_csv)<os.path.getmtime(fichier_wav) :
			extractHighestMagnitudes(fichier_wav, fichier_csv)
		else :
			print("Fichier csv déjà créé")	
	except OSError : 
		extractHighestMagnitudes(fichier_wav, fichier_csv)
	try :
		if os.path.getmtime(fichier_svm)<os.path.getmtime(fichier_csv) :
			convert(fichier_csv, fichier_svm, overlap, dimension, id)
		else :
			print("Fichier svm déjà créé")	
	except OSError :
		convert(fichier_csv, fichier_svm, overlap, dimension, id)
		
def create_svm(descripteur):
	dossier = os.path.join("data", descripteur, "svm")
	a = OAOManager()
	a.createSVM(dossier)
		
def update_svm(descripteur):
	dossier = os.path.join("data", descripteur, "svm")
	a = OAOManager()
	a.addMusic(dossier)

if __name__ == "__main__":
    #add_music_group_pcp(12,60, "Impromptu", ["data/wav/Impromptu op9n4 (1).wav", "data/wav/Impromptu op9n4 (2).wav"])
    
    #print(test_music_group(12,60,["data/wav/4_Marche_turque.wav"]))
    
	print(convert_mp3_to_wav(os.path.join("data", "wav", "4_Marche_turque.mp3")))
    
