#!/usr/bin/python3


from PyQt5 import QtGui, QtCore, QtWidgets, uic
import sys
import ast
import timeit
import os
import sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import FuncFormatter, MaxNLocator
from itertools import cycle
from time import sleep
from threading import Thread
from multiprocessing import Process
cycol = cycle('bgrcmk').__next__

DIR = os.path.split(os.path.split(os.path.abspath(__file__))[0])[0]

os.chdir(DIR)

sys.path.append(DIR)

import Music
import tools


OVERLAP = 12
DIMENSION = 60


class Ui(QtWidgets.QMainWindow):
    def __init__(self,parent=None):
        super().__init__()
        uic.loadUi(os.path.join(DIR, 'gui', 'interface.ui'),self)


        infos = tools.dictionnaire.load()
        for name in infos.values():
            item=QtWidgets.QTreeWidgetItem([name])
            self.bdd.addTopLevelItem(item)


        self.Button_recon.clicked.connect(self.recon)
        self.Button_add.clicked.connect(self.add)
        self.Button_remove.clicked.connect(self.remove)
        self.MainWindow=self.Button_add.window()

        self.err = QtWidgets.QErrorMessage(self.MainWindow)

    def add (self, *args, **kwargs):
        name, ok = QtWidgets.QInputDialog.getText(self, 'Input Dialog', 
        'Entrez le nom de votre musique:')
        if not ok :
            return
        fnames, _ = QtWidgets.QFileDialog.getOpenFileNames(self, 'Open file')

        #item = QtWidgets.QTreeWidgetItem(self.bdd)
        #item.setText(0,name)
        self.run_from_worker(Music.add_music_group_pcp, (OVERLAP, DIMENSION, name, fnames))
        item=QtWidgets.QTreeWidgetItem([name])
        self.bdd.addTopLevelItem(item)

        for file in fnames:
            item.addChild(QtWidgets.QTreeWidgetItem([file]))
        

    def remove(self, *args, **kwargs):
        root = self.bdd.invisibleRootItem()
        for item in self.bdd.selectedItems():
            if not item.parent():
                Music.remove_music_group_pcp(item.data(0,0))
                root.removeChild(item)
                
    def recon(self, *x, **y):
        fname, _ = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file')
        self.run_from_worker(Music.test_music_group_pcp, (OVERLAP, DIMENSION, [fname], ), affichage=show_results)

    def run_from_worker(self, calcul, args = tuple(), kwargs = dict(), affichage=None):
        self.worker = Worker(calcul, args, kwargs, affichage)
        self.workerThread = QtCore.QThread()
        self.worker.moveToThread(self.workerThread)

        self.worker.finished.connect(self.reenable)
        self.workerThread.started.connect(self.worker.run)
        
        # start the execution loop with the thread:
        self.MainWindow.setEnabled(False)
        self.workerThread.start()

        #Thread(target=_recon).start()
    def reenable(self):
        self.MainWindow.setEnabled(True)
    

class Worker(QtCore.QObject):
    'Object managing the simulation'

    finished = QtCore.pyqtSignal(name = 'finished')
    def __init__(self, calcul, args = tuple(), kwargs = dict(), affichage=None):
        super(Worker, self).__init__()
        self.calcul = calcul
        self.args = args
        self.kwargs = kwargs
        self.affichage = affichage

    def run(self):
            result = self.calcul(*self.args, **self.kwargs)
            print(result)
            self.finished.emit()
            if self.affichage and result:
                p = Process(target=self.affichage, args=(result,))
                p.start()
                p.join()


def format_fn(tick_val, tick_pos):
    labels = tools.dictionnaire.load()
    return labels.get(int(tick_val), "")

def show_results(data):
    fig, ax = plt.subplots()

    bar_width = 1/(len(data)+2)
    offset = -bar_width*((len(data)+1)//2)

    opacity = 0.4

    for desc, scores in data.items():
        scores = list(scores.items())
        index = np.array(tuple(map(lambda x:int(x[0]), scores)))
        values = np.array(tuple(map(lambda x:x[1], scores)))
        values = values/sum(values)
        print(index, values)
        if len(index):
            rects = plt.bar(
                        index + offset,
                        values,
                        bar_width,
                        color=cycol(),
                        alpha=opacity,
                        label=desc)
            offset += bar_width

    ax.xaxis.set_major_formatter(FuncFormatter(format_fn))
    ax.xaxis.set_major_locator(MaxNLocator(integer=True))
    plt.xlabel('Groupe (Musique)')
    plt.ylabel('Degré de ressemblance normalisé')
    plt.title("Ressemblance de l'échantillon aux musiques connues")
    plt.legend(title="Méthode de comparaison")

    plt.tight_layout()
    plt.show()



if __name__ == '__main__':

    import traceback
    from tkinter.messagebox import showerror
    for i in (("times",), ("csv",), ("svm", "add"), ("svm", "test"), ("atester",)):
        os.makedirs(os.path.join(DIR, "data","wavparser",  *i), exist_ok=True)

    app = QtWidgets.QApplication(sys.argv)
    window = Ui()
    window.show()
    def exchook(type, value, tb):
        p = Process(target=lambda *x: showerror(title='Error', message="An exception occured. Quitting.\n"+"".join(traceback.format_exception(type, value, tb))))
        p.start()
        sys.__excepthook__(type, value, tb)
        p.join()
        exit(2)
    sys.excepthook =  exchook
    sys.exit(app.exec_())
    
        




        
        
