#!/usr/bin/python3
# coding: utf-8

from .wavinput import WavFile
from .spectrum import get_scores
from .tempo import get_normalized_beats, find_tempo

from itertools import islice
import math
import sys
import csv

min_pitch = 21 # première note du piano
max_pitch = 108 # dernière note du piano


def freq2pitch(hz):
    return 69 + 12*math.log2(hz/440)

def create_csv(f_in, f_beats, f_out):
    with WavFile(f_in) as f:
        samplerate = f.samplerate
        samples = list(f.get_samples())


    with open(f_beats, "r") as f:
        beats = list(map(int, f.read().splitlines()))
    print("tempo", file=sys.stderr)
    tempo = find_tempo([i/samplerate for i in beats]) # ce calcul se fait en secondes

    start = 0
    stop = 0

    n_notes = 2

    n = 0
    last_row = None
    with open(f_out, "w") as out:
        writer = csv.writer(out)
        for start, stop, n1, n2 in get_normalized_beats(beats, tempo*samplerate):
            print("fft", round(start/samplerate, 2), "s", round(100*start/len(samples)), "%", file=sys.stderr)
            harmoniques = list(get_scores(samples[start:stop], samplerate))
            row = []
            for freq, amplitude in get_scores(samples[start:stop], samplerate, n_notes).items():
                height = (freq2pitch(freq) - min_pitch)/(max_pitch-min_pitch)
                row.append([height, amplitude])
            if len(row)==0: # silence
                row = last_row # prolonger artificiellement la dernière note
            if row is None: # silence depuis le début
                continue
            if len(row)<=n_notes:
                row = ([row[0]]*(n_notes-len(row)+1))+row[1:]
            last_row = row
            while n<n2:
                writer.writerow(sum(row, []))
                n+=1





if __name__=="__main__":
    f_in = sys.argv[1]
    f_beats = sys.argv[2] if len(sys.argv)>=3 else sys.argv[1].rsplit(".", 1)[0]+".times"
    f_out = sys.argv[3] if len(sys.argv)>=4 else sys.argv[1].rsplit(".", 1)[0]+".csv"
    create_csv(f_in, f_beats, f_out)

    
    
