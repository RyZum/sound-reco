#!/usr/bin/python3
# coding: utf-8

from .wavinput import WavFile
from .rythm import get_beat_indexes
import sys




def create_times(f_in, f_out):
    with WavFile(f_in) as f:
        samplerate = f.samplerate
        samples = list(f.get_samples())


    length = 0.1
    samplelength = round(length*samplerate)

    with open(f_out, 'w') as f:
        f.write("\n".join(map(str,get_beat_indexes(samples, samplelength))))
        
if __name__=="__main__":
    f_in = sys.argv[1]
    f_out = sys.argv[2] if len(sys.argv)>=3 else sys.argv[1].rsplit(".", 1)[0]+".times"
    create_times(f_in, f_out)


