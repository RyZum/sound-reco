#!/usr/bin/python3

import math
import statistics
import numpy as np
from collections import OrderedDict
from itertools import islice

from .wavinput import WavFile

QUART = 2**(1/24)
DEMI = QUART**2
TON = DEMI**2

FMIN = 20
FMAX = 18000

def get_scores(samples, samplerate, n_notes=2):
    length = len(samples)/samplerate
    spectrum = np.square(np.abs(np.fft.rfft(samples))/len(samples))
    power = sum(spectrum)
    threshold = power*1e-5

    maxes = OrderedDict()
    i_max = math.floor(min(len(spectrum), FMAX*length))
    i = math.ceil(FMIN*length)
    low = i/QUART
    high = i*QUART
    while i < i_max:
            if spectrum[i] > threshold and spectrum[i]==max(spectrum[math.floor(low):math.ceil(high)+1]):
                    maxes[i/length] = sum(spectrum[math.floor(low):math.ceil(high)+1])
                    low, high = i, i*DEMI
                    i=math.ceil(i*QUART)
            else:
                    i+=1
                    low = i/QUART
                    high = i*QUART



    families = {}
    temp = list(maxes)
    while temp:
            f = temp.pop(0)
            A = maxes[f]
            families[f] = [(f, A)]
            for f2 in maxes:
                    n = round(f2/f)
                    theo = n*f
                    if n>=1 and abs(12*math.log2(theo/f2)) <= 0.12 :
                            families[f].append((f2, maxes[f2]))
                            if f2 in temp:
                                temp.remove(f2)
    scores = {}
    for f in families:
        amplitude = sum(map(lambda x: x[1], families[f]))/power
        if amplitude > 0.05:
            real_score = max(families[f], key = lambda x: x[1])[0]
            scores[real_score] = amplitude

    scores = dict(islice(sorted(scores.items(), key = lambda x: x[1], reverse = True), n_notes))

    return scores

if __name__ == "__main__":

    with WavFile("gneu.wav") as f:
        samples = list(f.get_samples())
        for score, amplitude in get_scores(samples, f.samplerate).items():
            print(score, round(amplitude, 2))
