#!/usr/bin/python3
import numpy as np
import scipy.signal

delta = 0.1 #largeur des pics
dt = delta/10 #precision du calcul
max_tempo = 3 # secondes

def gen_clicktrack(beats):
    time = np.arange(-delta, max(beats)+delta, dt)
    x = []
    y = []
    for t in beats:
        x.append(t-delta)
        y.append(0)
        x.append(t)
        y.append(1)
        x.append(t+delta)
        y.append(0)
    return np.interp(time, x, y)

def slide_product(clicktrack):
    d_domain = min(round(max_tempo/dt), len(clicktrack)-1)
    res = np.zeros(d_domain)
    for d in range(d_domain):
        temp = 0
        for i in range(len(clicktrack)-d):
            temp+=clicktrack[i]*clicktrack[i+d]
        res[d]=temp
    return res

def find_tempo (beats):
    maxes = scipy.signal.argrelmax(slide_product(gen_clicktrack(beats)))[0]
    return maxes[0]*dt # en secondes

def get_normalized_beats( beats, tempo):
    t = 0
    n = 0
    for t2 in beats:
        n2 = n + round((t2-t)/tempo)
        if n2>n:
            yield (t, t2, n, n2)
        t, n = t2, n2

if __name__=="__main__":
    import sys
    beats = []
    with open(sys.argv[1], "r") as f:
        for line in f:
            beats.append(float(line.split("\t")[0]))
    clicktrack = gen_clicktrack(beats)
    res = slide_product(clicktrack)
    import matplotlib.pyplot as mp
    mp.plot(res)
    mp.show()


