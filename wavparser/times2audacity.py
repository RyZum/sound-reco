#!/usr/bin/python3
import sys
with open(sys.argv[1], "r") as f:
    for t in map(int, f):
        print(round(t/44100, 2), "s", "s")
