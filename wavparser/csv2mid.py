
#!/usr/bin/python3
# coding: utf-8

import sys
import csv

from .MidiFile3 import MIDIFile
from .generator import min_pitch, max_pitch

midi = MIDIFile(1) # 1 track 
track = 0
channel = 0
midi.addTempo(track,0,200) # track, time, beat per minute


with open(sys.argv[1], "r") as f:
    for i, row in enumerate(csv.reader(f)):
        pitches = map(lambda x: round((max_pitch-min_pitch)*float(x)+min_pitch), row[::2])
        volumes = map(lambda x: int(128*float(x)), row[1::2])
        for pitch, volume in zip(pitches, volumes):
            midi.addNote(track, channel, pitch, i, 1, volume) 
with open(sys.argv[1]+".mid", "wb") as out:
    midi.writeFile(out)
