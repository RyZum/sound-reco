#!/usr/bin/python3
# -*- encoding: utf-8 -*-


import numpy as np

def get_maxes(x, width):
    i=width
    max_index = len(x)-width-1
    def is_max_around(index):
        local = x[index]
        for i in range(index-width, index+width):
            if local<x[i]:
                return False
        return True
    while i<max_index:
        if i%10000==0:
            print("rythm", i/max_index)
        if is_max_around(i):
            yield i
            i+=width
        else:
            i+=1


def power(samples, width):
    return np.convolve(np.square(samples), np.ones(width), mode="same")

def get_beat_indexes(samples, resolution):
    last = -resolution-1
    for i in get_maxes(power(samples, resolution), resolution//2):
        if i-last>1*resolution:
            yield i
            last = i

if __name__ == "__main__":
    from wavinput import WavFile
    import sys
    import matplotlib.pyplot as mp
    with WavFile(sys.argv[1]) as f:
        samples = list(f.get_samples())
        length = 0.1
        samplelength = int(f.samplerate*length)
        for i in get_beat_indexes(samples, samplelength):
            print(i/f.samplerate, i/f.samplerate, "s", sep="\t")

