#!/usr/bin/python3
# -*- encoding: utf8 -*-

import wave
import audioop
class WavFile:
        def __init__(self, filename, buffer = 4096):
                self.filename = filename
                self.file = None
                self.buffer = buffer

        def __enter__(self):
                self.file = wave.open(self.filename, "rb").__enter__()
                (self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
                assert self.nchannels == 1, "Only mono wave files are supported"
                self.length = self.nsamples/self.samplerate
                self.max_amplitude = 1<<(8*self.samplewidth -1)
                return self

        def __exit__(self, *args, **kwargs):
                if self.file is not None:
                        self.file.__exit__(*args, **kwargs)
                        self.file = None

        def get_samples(self):
                """Renvoie les échantillons du fichier WAVE PCM signé spécifié sous la forme de réels entre -1 et 1"""
                frames = None
                nframes = self.nsamples*self.samplewidth
                while frames != "" and nframes:
                        frames = self.file.readframes(min(nframes, self.buffer*self.samplewidth))
                        nframes -= len(frames)
                        for index in range(len(frames)//self.samplewidth):
                                sample = audioop.getsample(frames, self.samplewidth, index)
                                yield sample/self.max_amplitude

if __name__ == "__main__":
        with WavFile("lacarre.wav") as f:
                for i in f.get_samples():
                        print(i)
