import wave
import audioop

import math
from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


class WavFile:
		def __init__(self, filename, dureeLecture, buffer = 4096):
				self.filename = filename
				self.file = None
				self.buffer = buffer
				self.dureeLecture = dureeLecture

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None

		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude


#Descripteur composé du centroïde spectral, de la largeur spectrale et de l'asymétrie spectrale.
def moments(tableau):
	d = len(tableau)
	tab_fft = fft(tableau)
	
	mu = list(range(0,4));
	mu[1] = 0
	mu[2] = 0
	mu[3] = 0
	sumCoeffs = 0
		
	for i in range(0, d):
		freq = i/d
		temp = abs(tab_fft[i])
		sumCoeffs += temp
		mu[1] += freq * temp
		mu[2] += freq * freq * temp
		mu[3] += freq * freq * freq * temp
	if sumCoeffs != 0:
		mu[1] = mu[1] / sumCoeffs
		mu[2] = mu[2] / sumCoeffs
		mu[3] = mu[3] / sumCoeffs
		
	spectralCentroid = mu[1]
	spectralWidth = math.sqrt(mu[2] - mu[1]*mu[1])
	spectralSkewness = 0
	if spectralWidth != 0:
		spectralSkewness = (2*mu[1]*mu[1]*mu[1] - 3*mu[1]*mu[2] + mu[3]) / (spectralWidth*spectralWidth*spectralWidth)
	
	return (np.abs(spectralCentroid), np.abs(spectralWidth), np.abs(spectralSkewness))
		
def ecrireTableau(tableau, adresseEcriture):
	with open(adresseEcriture, 'a') as fichier:  # en mode ajout au contenu déjà existant.
		#fichier.write('[')
		l = len(tableau)
		for i in range(0, l-1):			# cette définition signifie jusqu'à l-2. Python is weird...
			fichier.write(str(tableau[i]))
			fichier.write(',')
		fichier.write(str(tableau[l-1]))
		#fichier.write(']')
		fichier.write('\n')
		
def extractMoments(adresseFichier, adresseEcriture):
	fenetre = 0.1
	with WavFile(adresseFichier, fenetre) as fichier:
		sumC = 0
		sumW = 0
		sumS = 0
		c = list(range(0,300))
		w = list(range(0,300))
		s = list(range(0,300))
		for i in range(0, 300):
			fs = fichier.samplerate
			data = list(fichier.get_samples())
			(a,b,d) = moments(data)
			c[i] = a
			w[i] = b
			s[i] = d
			sumC += c[i]
			sumW += w[i]
			sumS += s[i]

		with open(adresseEcriture, 'a') as fichierEcriture:
			for i in range(0,300):
				if sumC != 0:
					c[i] = c[i] / sumC
				if sumW != 0:
					w[i] = w[i] / sumW
				if sumS != 0:
					s[i] = s[i] / sumS
				
				fichierEcriture.write(str(c[i]))
				fichierEcriture.write(',')
				fichierEcriture.write(str(w[i]))
				fichierEcriture.write(',')
				fichierEcriture.write(str(s[i]))
				fichierEcriture.write('\n')
				
#extractMoments("D:\PSC\Echantillons\Elise (2).wav", "D:\\PSC\\Code\\Python\\PCP\\test_resultat.txt")

def traiter(adresseLecture):
	dossier = "D:/PSC/Echantillons/selection/"		# là où sont les fichiers wav
	with open(adresseLecture, 'r') as lecture:
		i=0;
		for ligne in lecture:
			i = i + 1;
			adresseRelative = ligne[:-1]
			adresseAbsolue = dossier + adresseRelative   # pour enlever le \n à la fin
			print("debut traitement" + adresseAbsolue)
			extractMoments(adresseAbsolue, "D:/PSC/Echantillons/selection/" + adresseRelative + ".txt")
			print("fin traitement" + adresseAbsolue)

traiter("D:\PSC\Echantillons\selection\liste.txt")
