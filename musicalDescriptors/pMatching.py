import wave
import audioop

import math
from scipy.fftpack import fft
from scipy import signal
import numpy as np
import matplotlib.pyplot as plt

class WavFile:
		def __init__(self, filename, dureeLecture, buffer = 4096):
				self.filename = filename
				self.file = None
				self.buffer = buffer
				self.dureeLecture = dureeLecture

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None

		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude

def freq_from_autocorr(sig, fs):
	
	# Calculate autocorrelation (same thing as convolution, but with 
	# one input reversed in time), and throw away the negative lags
	corr = signal.fftconvolve(sig, sig[::-1], mode='full')
	corr = corr[len(corr)/2:]
	
	result = list(range(0,1))
	
    # Find the first maximum, corresponding (without considering the interpolation problems yet) to the fundamental period.
	peak = 0
	maxVal = 0
	for i in range(0, len(corr)):
		if(corr[i] > maxVal):
			maxVal = corr[i]
			peak = i
	if(peak!=0):
		result[0] = fs / peak
	else:
		result[0] = 0
		
	return result

def ecrireTableau(tableau, adresseEcriture):
	with open(adresseEcriture, 'a') as fichier:  # en mode ajout au contenu déjà existant.
		#fichier.write('[')
		l = len(tableau)
		for i in range(0, l-1):			# cette définition signifie jusqu'à l-2. Python is weird...
			fichier.write(str(tableau[i]))
			fichier.write(',')
		fichier.write(str(tableau[l-1]))
		#fichier.write(']')
		fichier.write('\n')
		
def extractMainF0(adresseFichier, adresseEcriture):
	fenetre = 0.1
	with open(adresseEcriture, 'a') as lieu:
		lieu.write(adresseFichier)		# On dit de quel extrait ça vient
		lieu.write('\n')
	### Ecrit dans le fichier à l'adresse donnée la suite des tableaux PCP du morceau.
	with WavFile(adresseFichier, fenetre) as fichier:
		for i in range(0, 300): #avant:50
			fs = fichier.samplerate
			data = list(fichier.get_samples())
			ecrireTableau(freq_from_autocorr(data, fs), adresseEcriture)

def traiter(adresseLecture, adresseEcriture):
	#dossier = "D:/PSC/Echantillons/selection/"
	dossier = "D:/PSC/Echantillons/grosDossier/audio/"			# là où sont les fichiers wav
	with open(adresseLecture, 'r') as lecture:
		i=0;
		for ligne in lecture:
			i = i + 1;
			adresseRelative = ligne[:-1]
			adresseAbsolue = dossier + adresseRelative   # pour enlever le \n à la fin
			print("debut traitement" + adresseAbsolue)
			#extractMainF0(adresseAbsolue, "D:/PSC/Echantillons/selection/" + adresseRelative + ".txt")
			extractMainF0(adresseAbsolue, adresseEcriture + adresseRelative + ".txt")
			print("fin traitement" + adresseAbsolue)

#traiter("D:\PSC\Echantillons\selection\liste.txt")
#traiter("D:\PSC\Echantillons\selectionModerne\liste.txt", "D:/PSC/Echantillons/selectionModerne/");	
traiter("D:/PSC/Echantillons/grosDossier/audio/liste.txt", "D:/PSC/Echantillons/grosDossier/audio/ResultatsAutocorr/");		
