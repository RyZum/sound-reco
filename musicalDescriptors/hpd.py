# -*- coding:Utf-8 -*-

import wave
import audioop

import math
from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


class WavFile:
		def __init__(self, filename, fenetre):
				self.filename = filename
				self.file = None
				self.dureeLecture = fenetre

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None
                                                
						
		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude
				
# On choisit les bandes de fréquence :
# (Low tones)
# 30-40 Hz
# 40-80 Hz
# 80-120 Hz
# (High tones)
# 120-180 Hz
# 180-300 Hz

# --> Total 5 boxes, corresponding to the tab tabMax(storing the max) and tabMaxFreq(storing the maximum frequency for the corresponding range).

def getHighestMagnitudes(tableau):
	tabFFT = fft(tableau)
	d = len(tabFFT)
	
	tabMax = list(range(0,5))
	tabMaxFreq = list(range(0,5))
	
	# 30-40
	tabMax[0] = 0.0
	for freq in range(30,40):
		magnitude = abs(tabFFT[freq])
		if(magnitude > tabMax[0]):
			tabMax[0] = magnitude
			tabMaxFreq[0] = freq
	# 40-80
	tabMax[1] = 0.0
	for freq in range(40, 80):
		magnitude = abs(tabFFT[freq])
		if(magnitude > tabMax[1]):
			tabMax[1] = magnitude
			tabMaxFreq[1] = freq

	# 80-120
	tabMax[2] = 0.0
	for freq in range(80, 120):
		magnitude = abs(tabFFT[freq])
		if(magnitude > tabMax[2]):
			tabMax[2] = magnitude
			tabMaxFreq[2] = freq
	
	# 120-180
	tabMax[3] = 0.0
	for freq in range(120, 180):
		magnitude = abs(tabFFT[freq])
		if(magnitude > tabMax[3]):
			tabMax[3] = magnitude
			tabMaxFreq[3] = freq	
	
	# 180-300
	tabMax[4] = 0.0
	for freq in range(180, 300):
		magnitude = abs(tabFFT[freq])
		if(magnitude > tabMax[4]):
			tabMax[4] = magnitude
			tabMaxFreq[4] = freq	
			
	return tabMax
	
def ecrireTableau(tableau, adresseEcriture):
	with open(adresseEcriture, 'a') as fichier:  # en mode ajout au contenu déjà existant.
		#fichier.write('[')
		l = len(tableau)
		for i in range(0, l-1):			# cette définition signifie jusqu'à l-2. Python is weird...
			fichier.write(str(tableau[i]))
			fichier.write(',')
		fichier.write(str(tableau[l-1]))
		#fichier.write(']')
		fichier.write('\n')
		
def extractHighestMagnitudes(adresseFichier, adresseEcriture):
	fenetre = 0.1
	### Ecrit dans le fichier à l'adresse donnée la suite d'instances du descripteur.
	with WavFile(adresseFichier, fenetre) as fichier:
		for i in range(0, 100): #avant:50
			data = list(fichier.get_samples())
			ecrireTableau(getHighestMagnitudes(data), adresseEcriture)

#Les lignes suivantes sont commentées car les chemins sont ceux sur l'ordi de Rapahël, ce qui rend l'interprétation impossible sur un autre ordi			
#~ def traiter(adresseLecture):
	#~ dossier = "D:/PSC/Echantillons/selection/"		# là où sont les fichiers wav
	#~ with open(adresseLecture, 'r') as lecture:
		#~ i=0;
		#~ for ligne in lecture:
			#~ i = i + 1;
			#~ adresseRelative = ligne[:-1]
			#~ adresseAbsolue = dossier + adresseRelative   # pour enlever le \n à la fin
			#~ print("debut traitement" + adresseAbsolue)
			#~ extractHighestMagnitudes(adresseAbsolue, "D:/PSC/Echantillons/selection/" + adresseRelative + ".txt")
			#~ print("fin traitement" + adresseAbsolue)
#~ 
#~ #extractHighestMagnitudes("D:\PSC\Echantillons\Elise (2).wav", "D:\\PSC\\Code\\Python\\PCP\\test_resultat.txt")
#~ 
#~ traiter("D:\PSC\Echantillons\selection\liste.txt")
