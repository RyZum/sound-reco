# -*- coding:Utf-8 -*-

import wave
import audioop

import math
from scipy.fftpack import fft
import numpy as np
import matplotlib.pyplot as plt


class WavFile:
		def __init__(self, filename, fenetre):
				self.filename = filename
				self.file = None
				self.dureeLecture = fenetre

		def __enter__(self):
				self.file = wave.open(self.filename, "rb").__enter__()
				(self.nchannels, self.samplewidth,  self.samplerate,  self.nsamples, self.comptype,  self.compname) = self.file.getparams()
				# assert self.nchannels == 1, "Only mono wave files are supported"
				self.length = self.nsamples/self.samplerate
				self.max_amplitude = 1<<(8*self.samplewidth -1)
				return self

		def __exit__(self, *args, **kwargs):
				if self.file is not None:
						self.file.__exit__(*args, **kwargs)
						self.file = None
						
		def get_samples(self):
			frames = self.file.readframes((int)(self.samplerate * self.dureeLecture))
			for index in range(len(frames)//self.samplewidth):
				sample = audioop.getsample(frames, self.samplewidth, index)
				yield sample/self.max_amplitude

#with WavFile("D:\PSC\Echantillons\\100bpm.wav", 10) as fichier:
with WavFile("D:\PSC\Echantillons\elise (2).wav", 10) as fichier:
	
	a = list(fichier.get_samples())
	print("fichier ouvert")
	fs = fichier.samplerate
	print(fs)
	
	d = len(a)
	
	listBpm = range(50,200,10)
	
	# Avec l'autocorrélation.
	
	maximum = 0.0
	bpm = 0
	
	for b in listBpm:
		decalage = (int)(4 * 60*fs/b)
		correl = 0.
		for k in range(1,d-decalage):
			correl += (a[k]*a[k+decalage])/(d-decalage)
		if(correl >= maximum):
			bpm = b
			maximum = correl

	print("avec l'autocorrelation :  ")
	print(bpm)
	print("\n")
	
	# Corrélation avec un peigne de dirac.
	
	fourier = fft(a)
	
	maximum = 0.0
	bpm = 0
	
	for b in listBpm:
		correl = 0.
		periodeDirac = 60*fs/b
		dirac = np.zeros(d)
		i=0
		while(i*periodeDirac < d):
			dirac[i*periodeDirac] = 1.
			i = i+1
		diracFour = fft(dirac)
		for k in range(0,d):
			correl += (fourier[k]*diracFour[k])/d
		if(correl >= maximum):
			bpm = b
			maximum = correl
			
	print("Avec les dirac :  ")
	print(bpm)
	print('\n')
	
			
