#!/usr/bin/python3
import csv

def convert(fichier_in, fichier_out, overlap, out_size, music_id):
    with open(fichier_in, "r", newline='') as f_in:
        with open(fichier_out, "w") as f_out:
            elements = csv.reader(f_in)
            buffer=[]
            for row in elements:
                if len(row)==0 :
                    continue
                assert overlap%len(row)==0
                assert out_size%len(row)==0
                for elt in row:
                    buffer.append(elt)
                if len(buffer)>=out_size:
                    f_out.write("{} {}\n".format(music_id, " ".join(("{}:{}".format(numero, elt) for numero, elt in enumerate(buffer[:out_size],start=1)))))
                    buffer = buffer[overlap:]


if __name__=="__main__":
    import sys
    from pathlib import Path
    import argparse
    parser = argparse.ArgumentParser(description='Convertit les fichiers csv en fichiers svm')
    parser.add_argument('f_in', type=str, help="fichier d'entree en .csv")
    parser.add_argument('overlap', type=int, help="de combien le programme avance à chaque ligne de sortie")
    parser.add_argument('out_size', type=int, help="la dimension des vecteurs de sortie")
    parser.add_argument('f_out', type=str, help="fichier de sortie", default="", nargs='?')
    args = parser.parse_args()
    assert args.overlap < args.out_size
    in_file = Path(args.f_in)
    assert in_file.exists()
    assert in_file.suffix
    id = in_file.stem
    out_file = Path(args.f_out) if args.f_out else in_file.parent / id
	


    convert(str(in_file.absolute()), str(out_file.absolute()), args.overlap, args.out_size, id)
