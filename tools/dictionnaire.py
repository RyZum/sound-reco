import json
import os.path

dictionnaire = os.path.join("data", "dict.json")
def load():
    try :
        with open(dictionnaire, 'r') as f :
            return {int(k):v for k,v in json.load(f).items()}
            
    except FileNotFoundError:
        return({})
    
def save(dico):
    with open(dictionnaire, 'w') as f :
        json.dump(dico, f)
    
    
    
